const { merge } = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa-react");

module.exports = (webpackConfigEnv, argv) => {
  return singleSpaDefaults({
    orgName: "psb",
    projectName: "react",
    webpackConfigEnv,
    argv,
  });
};
